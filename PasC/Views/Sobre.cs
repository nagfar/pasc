﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasC.Views
{
    /// <summary>
    /// Classe para exibir informações sobre o sistema.
    /// </summary>
    public partial class Sobre : Form
    {
        /// <summary>
        /// Inicialização dos componentes do sistema.
        /// </summary>
        public Sobre()
        {
            InitializeComponent();
        }
    }
}
