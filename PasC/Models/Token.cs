﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasC.Models
{
    /// <summary>
    /// Classe estática contendo todos os tokens da linguagem
    /// </summary>
    public static class Token
    {
        /// <summary>
        /// Nome do token
        /// </summary>
        public static string Nome { get; set; }
        /// <summary>
        /// Valor do token
        /// </summary>
        public static string Valor { get; set; }
        public const string OP_EQ = "OP_EQ";
        public const string OP_NE = "OP_NE";
        public const string OP_GT = "OP_GT";
        public const string OP_LT = "OP_LT";
        public const string OP_GE = "OP_GE";
        public const string OP_LE = "OP_LE";
        public const string OP_AD = "OP_AD";
        public const string OP_MIN = "OP_MIN";
        public const string OP_MUL = "OP_MUL";
        public const string OP_DIV = "OP_DIV";
        public const string OP_ASS = "OP_ASS";
        public const string SMB_OBC = "SMB_OBC";
        public const string SMB_CBC = "SMB_CBC";
        public const string SMB_OPA = "SMB_OPA";
        public const string SMB_CPA = "SMB_CPA";
        public const string SMB_COM = "SMB_COM";
        public const string SMB_SEM = "SMB_SEM";
        public const string ID = "ID";
        public const string CHAR_CONST = "CHAR_CONST";
        public const string CON_NUM = "CON_NUM";
    }
}
